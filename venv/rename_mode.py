from enum import Enum


class RenameMode(Enum):
    NAME_AND_NUMBER = 0
    NUMBER_AND_NAME = 1
