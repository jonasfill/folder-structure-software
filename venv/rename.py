import os
from rename_mode import *
from separation_mode import *
from ordering_mode import *
import utility
from get_image_metadata import *
import functools


def rename_files(path, rename_mode, separation_mode, sectors, borders, names, cont, ordering_mode, start, end):
    for subdir, dirs, files in os.walk(path):
        # remove DS_Store file for macOS
        if ".DS_Store" in files:
            files.remove(".DS_Store")
        # digits of filenumber (needed for determination of leading zeros needed
        digits = utility.get_digits(len(files))
        # Sort depending on ordering mode
        if OrderingMode(ordering_mode) == OrderingMode.NAME:
            # Files sorted by name
            sorted_ = sorted(files)
        elif OrderingMode(ordering_mode) == OrderingMode.DATE:
            sorted_ = sorted(files, key=lambda x: get_date_taken(subdir + "/" + x))
        if sectors == 1:
            for i in range(start-1, end):
                rename_file(subdir, sorted_[i], i+1, rename_mode, separation_mode, names[0], digits)
        else:
            # append end of list to borders
            borders.append(len(sorted_))
            # Current 'sector border' to watch on
            border_index = 0
            number = 0
            for i in range(start-1, end):
                if i >= borders[border_index]:
                    # set border index one position further and reset current number
                    border_index += 1
                    number = 0
                if cont:
                    rename_file(subdir, sorted_[i], i+1, rename_mode, separation_mode, names[border_index], digits)
                else:
                    rename_file(subdir, sorted_[i], number+1, rename_mode, separation_mode, names[border_index], digits)
                number += 1


def rename_file(subdir, f, i, rename_mode, separation_mode, name, num_length):
    dot_index = f.rfind(".")
    if dot_index != -1:
        # if filename contains a dot (has ending), extract ending
        ending = f[dot_index:]
    else:
        ending = ""
    if RenameMode(rename_mode) == RenameMode.NAME_AND_NUMBER:
        # Separate name and number using the given separation mode
        new_name = separate_name(name, str(i).zfill(num_length), separation_mode)
        if os.path.exists(subdir + "/" + new_name):
            """
            This means that we try to rename something to an already existing file in
            the folder. This usually means the files have a bad naming convention
            """
            raise NameError("Trying invalid renaming")
        # Rename file
        os.rename(subdir + "/" + f, subdir + "/" + new_name + ending)
    elif RenameMode(rename_mode) == RenameMode.NUMBER_AND_NAME:
        new_name = separate_name(str(i).zfill(num_length), name, separation_mode)
        if os.path.exists(subdir + "/" + new_name):
            """
            This means that we try to rename something to an already existing file in
            the folder. This usually means the files have a bad naming convention
            """
            raise NameError("Trying invalid renaming")
        os.rename(subdir + "/" + f, subdir + "/" + new_name + ending)


def separate_name(first, second, separation_mode):
    if SeparationMode(separation_mode) == SeparationMode.SPACE_SEPARATED:
        return "{0} {1}".format(first, second)
    elif SeparationMode(separation_mode) == SeparationMode.UNDERSCORE_SEPARATED:
        return "{0}_{1}".format(first, second)
    elif SeparationMode(separation_mode) == SeparationMode.DASH_SEPARATED:
        return "{0}-{1}".format(first, second)
    elif SeparationMode(separation_mode) == SeparationMode.NOT_SEPARATED:
        return "{0}{1}".format(first, second)
