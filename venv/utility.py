from difflib import SequenceMatcher

units = ["B", "KiB", "MiB", "GiB", "TiB"]


def readable_size(size):
    # current unit of measurement
    i = 0
    while size//1024 != 0:
        size /= 1024
        i += 1
    return "{0} {1}".format(round(size, 2), units[i])


def strings_similar(first, second):
    similarity_full = similar(first, second)
    similarity_head = similar(first[0:5], second[0:5])
    acronym = first == ''.join([x[0] for x in second.split()]) or second == ''.join([x[0] for x in first.split()])
    """
    Strings are seen as similar if one of the following criteria is met
    1) the full strings are fairly similar
    2) the beginnings of the strings are very similar
    3) one string is an acronym of the other one
    """
    if similarity_full >= 0.6 or similarity_head >= 0.65 or acronym:
        return True
    return False


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def first_upper(string):
    # Convert the given string to a string starting with an uppercase-character
    return string[0].upper() + string[1:len(string)]


def first_lower(string):
    # Convert the given string to a string starting with an lowercase-character
    return string[0].lower() + string[1:len(string)]


def separate_words(string):
    # Separate string at every uppercase-character
    final_list = []
    to_slice = string
    i = 0
    while i < len(to_slice):
        if to_slice[i].isupper() and len(to_slice[:i]) >= 1:
            # if we find an uppercase character, separate string and append first part to resulting list
            final_list.append(to_slice[:i])
            to_slice = to_slice[i:]
            i = 0
        i += 1
    final_list.append(to_slice)
    # Only return final list if the string contains few uppercase-characters relative to the total number of characters
    if len(final_list) / len(string) < 0.3:
        return final_list
    else:
        return [string]


def get_digits(input_num):
    count = 0
    while input_num > 0:
        count += 1
        input_num //= 10
    return count
