from enum import Enum


class FormatMode(Enum):
    UPPER_BEGIN = 0
    LOWER_BEGIN = 1
    CAMEL_CASE = 2
    DEFAULT = 3
