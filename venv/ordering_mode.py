from enum import Enum


class OrderingMode(Enum):
    NAME = 0
    DATE = 1
