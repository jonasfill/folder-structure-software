#!/usr/bin/env python3

from utility import *
from format_mode import *
from separation_mode import *
from rename_mode import *
from heap import *
from inspection import *
from format import *
from rename import *


def print_help():
    print()
    print("q - Programm beenden")
    print("o - Dateien eines Ordners inklusive aller unterordner der Größe nach auflisten")
    print("h - Hilfe")
    print("i - Inspektion des Ordnerinhaltes und ggf. Warnungen")
    print("f - Dateinamen im Ordner formatieren")
    print("r - Dateinamen im Ordner umbenennen und fortlaufend nummerieren")
    print()


def print_separation_help():
    print()
    print("s - Leerzeichentrennung")
    print("u - Trennung durch Unterstrich")
    print("d - Trennung durch Bindestrich")
    print("n - Keine Trennung")
    print()


def print_format_help():
    print()
    print("u - Durchgängige Großschreibung")
    print("l - Durchgängige Kleinschreibung")
    print("c - 'Camel-Case' (erster Wort beginnt mit Großbuchstabe, restliche Wörter mit Kleinbuchstaben")
    print("x - Format beibehalten")
    print()


def input_path():
    path = input("Geben Sie den Pfad des Ordners ein:")
    if os.path.exists(path):
        return path
    else:
        print()
        print("Ungültiger Pfad. Bitte versuchen Sie es erneut")
        input_path()


print_help()
while 1:
    user_input = input("Was möchten Sie tun? (h - Hilfe)")
    # Command to quit program
    if user_input == "q":
        print("Aufwiedersehen")
        break
    # Command to observe directory
    elif user_input == "o":
        print()
        path = input_path()
        q = create_heap(path)
        list_ = heap_to_list(q)
        print()
        print_list(list_)
        print()
        # Move files to trash
        while 1:
            wants_to_del = input("Möchten Sie eine Datei in den Papierkorb verschieben? (y/n)")
            print()
            if wants_to_del == "y":
                name = input("Geben Sie den Namen der Datei ein:")
                delete_file(list_, name)
            else:
                break
            print()
    elif user_input == "i":
        print()
        path = input_path()
        print()
        wants_warnings = input("Möchten Sie Warnungen ausgeben lassen? (y/n)")
        print()
        if wants_warnings == "y":
            for w in inspect(path):
                wants_to_merge = input("Möglicherweise redundantes Unterverzeichnis in {0}. Wie möchten Sie vorgehen? "
                                     "(i - ignorieren, m - zusammenfügen)".format(w))
                if wants_to_merge == "m":
                    # remove parent directory and replace it with "only child"
                    merge_dir(w[0], w[1])
                elif wants_to_merge == "i":
                    continue
                print()
    elif user_input == "f":
        print()
        path = input_path()
        # Let user select separation mode
        while 1:
            print_separation_help()
            separation_mode = input("Bitte wählen Sie, wie Wörter getrennt werden sollen:")
            if separation_mode == "s":
                separation_mode = SeparationMode.SPACE_SEPARATED
            elif separation_mode == "u":
                separation_mode = SeparationMode.UNDERSCORE_SEPARATED
            elif separation_mode == "d":
                separation_mode = SeparationMode.DASH_SEPARATED
            elif separation_mode == "n":
                separation_mode = SeparationMode.NOT_SEPARATED
            else:
                print("Ungültige Auswahl. Bitte versuchen Sie es erneut.")
                continue
            break
        # Let user select format mode
        while 1:
            print_format_help()
            format_mode = input("Bitte wählen Sie den Formatierungsmodus aus:")
            if format_mode == "u":
                format_mode = FormatMode.UPPER_BEGIN
            elif format_mode == "l":
                format_mode = FormatMode.LOWER_BEGIN
            elif format_mode == "c":
                format_mode = FormatMode.CAMEL_CASE
            elif format_mode == "x":
                format_mode = FormatMode.DEFAULT
            else:
                print("Ungültige Auswahl. Bitte versuchen Sie es erneut.")
                continue
            break
        # Let user select recursive behaviour
        while 1:
            print()
            rec = input("Auf welche Verzeichnisinhalte soll die Änderung angewandt werden?\n"
                        "a - alle Verzeichnisinhalte inklusive der Unterverzeichnisse\n"
                        "c - nur direktes Unterverzeichnis\n")
            if rec == "a":
                rec = True
            elif rec == "c":
                rec = False
            else:
                print("Undefiniertes Verhalten. Bitte versuchen Sie es erneut.")
                continue
            break
        # Let user specify affected files
        while 1:
            print()
            affected_files = input("Welche Arten von Verzeichnisinhalten sollen von der Änderung betroffen sein?\n"
                                   "f - nur gewöhnliche Dateien\n"
                                   "d - nur Ordner\n"
                                   "a - alle Verzeichnisinhalte\n")
            if affected_files == "f":
                format_files = True
                format_dirs = False
            elif affected_files == "d":
                format_files = False
                format_dirs = True
            elif affected_files == "a":
                format_files = format_dirs = True
            else:
                print("Ungültige Eingabe. Bitte versuchen Sie es erneut.")
                continue
            break
        # Let user specify behaviour with hidden files
        while 1:
            print()
            hidden_beh = input("Erweiterte Einstellung. Sollen auch versteckte Dateien betroffen sein? (y/n):")
            if hidden_beh == "y":
                hidden = True
            elif hidden_beh == "n":
                hidden = False
            else:
                print("Ungültige Eingabe. Bitte versuchen Sie es erneut.")
                continue
            break
        # Call appropriate function to perform rename operation
        if rec:
            format_rec(path, format_mode, separation_mode, format_files, format_dirs, hidden)
        else:
            format_first(path, format_mode, separation_mode, format_files, format_dirs, hidden)
        print()

    elif user_input == "r":
        print()
        path = input_path()
        # Let user specify rename format
        while 1:
            print()
            rename_mode = input("Wie soll das Format der Dateien aussehen?\n"
                                   "e - Name, Nummer\n"
                                   "a - Nummer, Name\n")
            if rename_mode == "e":
                rename_mode = RenameMode.NAME_AND_NUMBER
            elif rename_mode == "a":
                rename_mode = RenameMode.NUMBER_AND_NAME
            else:
                print("Ungültiges Format. Bitte versuchen Sie es erneut.")
                continue
            break
        # Let user specify separation format
        while 1:
            print_separation_help()
            separation_mode = input("Wie sollen Dateiname und Nummer getrennt werden?")
            if separation_mode == "s":
                separation_mode = SeparationMode.SPACE_SEPARATED
            elif separation_mode == "u":
                separation_mode = SeparationMode.UNDERSCORE_SEPARATED
            elif separation_mode == "d":
                separation_mode = SeparationMode.DASH_SEPARATED
            elif separation_mode == "n":
                separation_mode = SeparationMode.NOT_SEPARATED
            else:
                print()
                print("Ungültige Auswahl. Bitte versuchen Sie es erneut.")
                print()
                continue
            print()
            break
        # Let user specify the number of sectors for renaming
        while 1:
            sectors = input("Wie viele Abschnitte wollen sie für die Umbenennung verwenden?")
            try:
                sectors = int(sectors)
            except ValueError:
                print()
                print("Bitte geben Sie eine Ganzzahl ein.")
                print()
                continue
            if sectors < 1:
                print()
                print("Bitte geben Sie eine Ganzzahl ein.")
                print()
                continue
            print()
            break
        # Let user specify the borders between sectors
        borders = list()
        for i in range(sectors - 1):
            input_ = input("Bitte geben Sie an, welche Nummer das letzte Element von Sektor {0} haben soll".format(i+1))
            while 1:
                try:
                    borders.append(int(input_))
                    break
                except ValueError:
                    print("Bitte geben Sie eine Ganzzahl ein")
        # Let user specify the filename(s)
        names = list()
        if sectors == 1:
            names.append(input("Bitte geben Sie den gemeinsamen Namensteil der Dateien ein:"))
        else:
            print()
            for i in range(sectors):
                names.append(input("Bitte geben Sie den Namen des Abschnittes {0} ein:".format(i+1)))
        while 1:
            # Let user specify if the numbering should be continuous
            print()
            cont = input("Soll die Nummerierung der Dateien durchgehend über Sektorgrenzen hinweg sein? (y/n)")
            print()
            if cont == "y":
                cont = True
            elif cont == "n":
                cont = False
            else:
                print("Ungültige Auswahl. Bitte versuchen Sie es erneut.")
                print()
                continue
            break
        rename_files(path, rename_mode, separation_mode, sectors, borders, names, cont)

    elif user_input == "h":
        print_help()
