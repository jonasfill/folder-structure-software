import os
import sys
from iheapq import *
from utility import *


def create_heap(_path):
    queue = []
    for subdir, dirs, files, fd in os.fwalk(_path):
        for f in files:
            # Determine size of all files and insert them into heap
            size = os.stat(f, dir_fd=fd).st_size
            # Push triple consisting of: (filesize, filename, path)
            heappush(queue, (size, f, subdir + "/" + f))
    return queue


def print_list(list_):
    for size, name, subdir in list_:
        print(readable_size(size), name, subdir)

def convert_list_to_strings(list_):
    result = list()
    for size, name, subdir in list_:
        result.append(readable_size(size) + "\t" + name + "\t" + subdir)
    return result


def heap_to_list(heap):
    list_ = []
    for i in range(len(heap)):
        list_.append(heappop(heap))
    return list_


def delete_file(list_, name):
    # list containing all elements matching the given filename
    tmp = [l for l in list_ if l[1] == name]
    if len(tmp) == 0:
        print("Keine Datei mit diesem Namen gefunden")
    else:
        # Move first element matching the given filename to trash
        size, filename, path_ = tmp[0]
        # macOS-version
        trash_macos = "/.Trash/"
        # linux version
        trash_linux = "/.local/share/Trash/files/"
        trash_filename = "{0}{1}".format(os.path.expanduser("~"), trash_macos) + filename
        # Create new filename which isn't already in the trash folder
        if os.path.exists(trash_filename):
            i = 1
            while os.path.exists(trash_filename):
                trash_filename = trash_filename + str(i)
                i += 1
        os.rename(path_, trash_filename)
