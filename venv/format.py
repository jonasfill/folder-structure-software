import os
from format_mode import *
from separation_mode import *
import functools

import utility


def format_first(path, format_mode, separation_mode, format_files, format_dirs, hidden):
    # Only performs change to direct subdirectory
    for subdir, dirs, files in os.walk(path):
        if format_files:
            rename(subdir, files, format_mode, separation_mode, hidden)
        if format_dirs:
            rename(subdir, dirs, format_mode, separation_mode, hidden)
        break


def format_rec(path, format_mode, separation_mode, format_files, format_dirs, hidden):
    # Performs change recursively to every subdirectory
    for subdir, dirs, files in os.walk(path, topdown=False):
        if format_files:
            rename(subdir, files, format_mode, separation_mode, hidden)
        if format_dirs:
            rename(subdir, dirs, format_mode, separation_mode, hidden)


def rename(subdir, files, format_mode, separation_mode, hidden):
    for f in files:
        # for macOS
        if f == ".DS_Store":
            continue
        if f.startswith('.'):
            if not hidden:
                continue
            else:
                # remove starting dot
                f = f[1:]
                input_ = standardize_input(f)
                new_name = convert(input_, format_mode, separation_mode)
                # Rename file
                os.rename(subdir + "/." + f, subdir + "/." + new_name)
        else:
            # Case for standard files
            input_ = standardize_input(f)
            new_name = convert(input_, format_mode, separation_mode)
            # Rename file
            os.rename(subdir + "/" + f, subdir + "/" + new_name)


def convert(input_, format_mode, separation_mode):
    # Apply formatting
    if FormatMode(format_mode) == FormatMode.UPPER_BEGIN:
        # Transform first letters to upper-case
        modified = [utility.first_upper(word) for word in input_]
    elif FormatMode(format_mode) == FormatMode.LOWER_BEGIN:
        # Transform first letters to lower-case
        modified = [utility.first_lower(word) for word in input_]
    elif FormatMode(format_mode) == FormatMode.CAMEL_CASE:
        # Transform letters to upper case except for first word
        modified = [utility.first_lower(input_[0])] + [utility.first_upper(word) for word in input_[1:len(input_)]]
    else:
        modified = input_

    # Apply separation
    if SeparationMode(separation_mode) == SeparationMode.SPACE_SEPARATED:
        return ' '.join(modified)
    elif SeparationMode(separation_mode) == SeparationMode.UNDERSCORE_SEPARATED:
        return '_'.join(modified)
    elif SeparationMode(separation_mode) == SeparationMode.DASH_SEPARATED:
        return '-'.join(modified)
    elif SeparationMode(separation_mode) == SeparationMode.NOT_SEPARATED:
        return ''.join(modified)


def standardize_input(name):
    # convert filename of arbitrary format to a list consisting of the single words
    list_ = list()
    list_.append(name.split(' '))
    list_.append(name.split('_'))
    list_.append(name.split('-'))
    # only check for separation by upper case letters if we found no other useful separation
    max_length = 1
    for l in list_:
        if len(l) > max_length:
            max_length = len(l)

    if max_length == 1:
        list_.append(utility.separate_words(name))
    # Get list with maximal length using a lambda-expression
    """
    The idea is that the list with the maximal length most likely contains the kind of separation
    intended by the creator
    """
    final_list = functools.reduce(lambda a, b: a if len(a) > len(b) else b, list_)
    return final_list
