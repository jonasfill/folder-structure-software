from utility import *
import os
import shutil

# Maximum number of files a folder can contain. In macOS 1 as every folder contains .DS-Store

# Linux
# MAX_FILES_IN_FOLDER = 0
# macOS
MAX_FILES_IN_FOLDER = 1


def inspect(path_):
    # Warnings caused by directory having only one subdirectory with similar name (similar subdirectory = ssd)
    warnings_ssd = []
    for subdir, dirs, files, fd in os.fwalk(path_):
        # Get name of the parent directory
        subdir_name = subdir.split('/')[-1]
        # Do we have to create an ssd-warning?
        if len(dirs) == 1 and len(files) <= MAX_FILES_IN_FOLDER and strings_similar(subdir_name, dirs[0]):
            warnings_ssd.append((subdir, dirs[0]))
    return warnings_ssd


def merge_dir(parent_dir, child):
    # Rename parent directory slightly
    i = 0
    new_path = parent_dir + str(i)
    while os.path.exists(new_path):
        i += 1
        new_path = parent_dir + str(i)
    os.rename(parent_dir, new_path)
    # Move child directory one level up
    child_dir = new_path + "/" + child
    os.rename(child_dir, new_path + "/../" + child)
    shutil.rmtree(new_path)
