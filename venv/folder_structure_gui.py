import wx
from format import *
from rename import *
from heap import *
from inspection import *
import os
import PIL

format_separation_text = "Bitte wählen Sie, wie Wörter getrennt werden sollen"
rename_separation_text = "Bitte wählen Sie, wie Dateiname und Nummer getrennt werden sollen"


def path_input(panel):
    panel.path = wx.BoxSizer(wx.HORIZONTAL)
    panel.path_label = wx.StaticText(panel,
        label="Geben Sie den Pfad des Ordners ein")
    panel.path_entry = wx.TextCtrl(panel, value="")
    panel.path_button = button(panel, "Durchsuchen")

    panel.Bind(wx.EVT_BUTTON, panel.EvtPathButton, panel.path_button)

    panel.path.Add(panel.path_label, 3, wx.TOP)
    panel.path.Add(panel.path_entry, 3, wx.TOP)
    panel.path.Add(panel.path_button, 1, wx.TOP)

def word_separation(panel, text):
    # Radiobox
    separation_list = ["Leerzeichentrennung", "Trennung durch Unterstrich",
        "Trennung durch Bindestrich", "Keine Trennung"]
    # You can set the maximum dimension with attribute majorDimension
    panel.separation = wx.RadioBox(panel,
        label=text,
        choices=separation_list, style=wx.RA_SPECIFY_COLS)

def word_format(panel):
    # Radiobox
    format_list = ["Durchgängige Großschreibung", "Durchgängige Kleinschreibung",
    "'Camel-Case'", "Format beibehalten"]
    # You can set the maximum dimension with attribute majorDimension
    panel.format = wx.RadioBox(panel,
        label="Bitte wählen Sie den Formatierungsmodus aus",
        choices=format_list, style=wx.RA_SPECIFY_COLS)

def change_recursive(panel):
    # Checkbox
    panel.change_recursive = wx.CheckBox(panel, label="Alle Unterverzeichnisse durchsuchen")

def file_types(panel):
    # Checkbox
    panel.folders = wx.CheckBox(panel, label="Ordner formatieren")
    panel.normal_files = wx.CheckBox(panel, label="Gewöhnliche Dateien formatieren")


def rename_mode(panel):
    # Radiobox
    rename_mode_list = ["Name, Nummer", "Nummer, Name"]
    # You can set the maximum dimension with attribute majorDimension
    panel.rename_mode = wx.RadioBox(panel,
        label="Wie soll das Format der Dateien aussehen?",
        choices=rename_mode_list, style=wx.RA_SPECIFY_COLS)

def ordering_mode(panel):
    # Radiobox
    ordering_list = ["Name", "Aufnahmedatum (nur für Dateitypen, die eine solche Information besitzen)"]
    # You can set the maximum dimension with attribute majorDimension
    panel.ordering_mode = wx.RadioBox(panel,
        label="In welcher Reihenfolge soll die Nummerierung erfolgen?",
        choices=ordering_list, style=wx.RA_SPECIFY_COLS)

def hidden(panel):
    panel.hidden = wx.CheckBox(panel, label="Erweiterte Einstellung: sollen auch versteckte Dateien betroffen sein?")

def sectors_num(panel):
    panel.sectors_num = wx.BoxSizer(wx.HORIZONTAL)
    panel.sectors_num_label = wx.StaticText(panel,
        label="Wie viele Abschnitte wollen sie für die Umbenennung verwenden?")
    panel.sectors_num_entry = wx.SpinCtrl(panel, value="1", min=1)
    panel.sectors_num.Add(panel.sectors_num_label, 5, wx.TOP)
    panel.sectors_num.Add(panel.sectors_num_entry, 1, wx.TOP)

def over_sectors(panel):
    # Radiobox
    panel.over_sectors = wx.CheckBox(panel, label="Dateien durchgehen über Sektorgrenzen hinweg nummerieren")

def files_box(panel):
    files_list = []
    panel.files_box = wx.ListBox(panel, size = (100,-1), choices = files_list, style = wx.LB_SINGLE)

def dialog_border(panel, i, min, max):
    message = "Bitte geben Sie an, welche Nummer der letzte Abschnitt von Abschnitt {0} haben soll".format(i)
    return wx.GetNumberFromUser(message,
        "Letzte Zahl", "Grenze eingeben", 0, parent=panel, min=min, max=max)

def dialog_first_elem(panel, min, max):
    message = "Bitte geben Sie an, bei welchem Element der Umbenennungsvorgang starten soll (einschließlich)"
    return wx.GetNumberFromUser(message,
                                "Start", "Start eingeben", 1, parent=panel, min=min, max=max)

def dialog_last_elem(panel, min, max):
    message = "Bitte geben Sie an, bei welchem Element der Umbenennungsvorgang enden soll (einschließlich)"
    return wx.GetNumberFromUser(message,
                                "Ende", "Ende eingeben", max, parent=panel, min=min, max=max)

def dialog_sector_names(panel, message):
    dialog_sector_names = wx.TextEntryDialog(panel, message, caption="Name des Sektors", value="Eingabe")
    dialog_sector_names.ShowModal()
    dialog_sector_names.Destroy()
    return dialog_sector_names.GetValue()

def dialog_yes_no(panel, message, caption):
    dialog_yes_no = wx.MessageDialog(panel, message, caption, style=wx.CANCEL|wx.YES_NO)
    sm = dialog_yes_no.ShowModal()
    dialog_yes_no.Destroy()
    return sm

def dialog_ok(panel, message, caption):
    dialog_ok = wx.MessageDialog(panel, message, caption, style=wx.OK)
    sm = dialog_ok.ShowModal()
    dialog_ok.Destroy()

def dialog_dir(panel, message):
    dialog_dir = wx.DirDialog(panel, message)
    dialog_dir.ShowModal()
    dialog_dir.Destroy()
    return dialog_dir.GetPath()

def button(panel, text):
    return wx.Button(panel, label=text)

class OutputFiles(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.list_ = list()
        # Create size
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        # Create path input field
        path_input(self)
        files_box(self)

        self.Bind(wx.EVT_LISTBOX, self.EvtFilesBoxSelect, self.files_box)

        self.button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.button_search = button(self, "Durchsuchen")
        self.button_delete = button(self, "Datei löschen")
        self.button_delete.Disable()
        self.button_sizer.Add(self.button_search, 1, wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, 20)
        self.button_sizer.Add(self.button_delete, 1, wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, 20)

        self.Bind(wx.EVT_BUTTON, self.EvtButtonSearch, self.button_search)
        self.Bind(wx.EVT_BUTTON, self.EvtButtonDelete, self.button_delete)

        self.sizer.Add(self.path, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.files_box, 4, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.button_sizer, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)

        # Set sizer
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.sizer.Fit(self)
        self.Show()

    def EvtButtonSearch(self, event):
        # Create list with sorted files
        if not os.path.exists(self.path_entry.GetValue()):
            dialog_ok(self, "Der angegebene Pfad existiert nicht.", "Fehler")
        else:
            q = create_heap(self.path_entry.GetValue())
            self.list_ = heap_to_list(q)
            # Append files to listbox
            # files_box(self)
            if not self.list_:
                dialog_ok(self, "Der angegebene Ordner beinhaltet keine aufzulistenden Dateien.", "Keine Dateien gefunden")
            else:
                self.files_box.InsertItems(convert_list_to_strings(self.list_), 0)

    def EvtButtonDelete(self, event):
        index_to_delete = self.files_box.GetSelection()
        name = self.list_[index_to_delete][1]
        wants_to_del = dialog_yes_no(self,
            "Möchten Sie die Datei \"{0}\" in den Papierkorb verschieben?".format(name),
            "Datei löschen")
        if wants_to_del == wx.ID_NO:
            return
        else:
            delete_file(self.list_, name)
            # Delete file from box and from actual list too
            self.files_box.Delete(index_to_delete)
            del self.list_[index_to_delete]

    def EvtFilesBoxSelect(self, event):
        # Enable delete button
        self.button_delete.Enable()

    def EvtPathButton(self, event):
        self.path_entry.SetValue(dialog_dir(self, "Wählen Sie den Ordner"))

class Inspect(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        # Create size
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        # Create path input field
        path_input(self)
        self.button_search = button(self, "Durchsuchen")

        self.Bind(wx.EVT_BUTTON, self.EvtButtonSearch, self.button_search)

        self.sizer.Add(self.path, 5, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.button_search, 1, wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, 20)

        # Set sizer
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.sizer.Fit(self)
        self.Show()

    def EvtButtonSearch(self, event):
        if not os.path.exists(self.path_entry.GetValue()):
            dialog_ok(self, "Der angegebene Pfad existiert nicht.", "Fehler")
        else:
            warnings = inspect(self.path_entry.GetValue())
            # If list is empty, print message dialog
            if not warnings:
                dialog_ok(self, "Keine Warnungen gefunden", "Keine Warnungen")
            else:
                for w in warnings:
                    wants_to_merge = dialog_yes_no(self, "Möglicherweise redundantes Unterverzeichnis in {0}: {1}. "
                        "Möchten Sie es mit dem Elternverzeichnis zusammenfügen?".format(w[0], w[1]),
                        "Redundantes Unterverzeichnis")
                    if wants_to_merge == wx.ID_YES:
                        merge_dir(w[0], w[1])

    def EvtPathButton(self, event):
        self.path_entry.SetValue(dialog_dir(self, "Wählen Sie den Ordner"))

class FormatFolder(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        # Create size
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        # Create path input field
        path_input(self)
        word_separation(self, format_separation_text)
        word_format(self)
        change_recursive(self)
        file_types(self)
        hidden(self)

        self.button_perform_change = button(self, "Dateien formatieren")
        self.Bind(wx.EVT_BUTTON, self.EvtButtonPerformChange, self.button_perform_change)

        self.sizer.Add(self.path, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.separation, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.format, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.change_recursive,1, wx.EXPAND)
        # self.sizer.Add(self.change_recursive, pos=(4,0), span=(1,2), flag=wx.BOTTOM, border=5)
        self.sizer.Add(self.folders, 1, wx.EXPAND)
        self.sizer.Add(self.normal_files, 1, wx.EXPAND)
        self.sizer.Add(self.hidden, 1, wx.EXPAND)
        self.sizer.Add(self.button_perform_change, 1, wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, 20)

        # Set sizer
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.sizer.Fit(self)
        self.Show()

    def EvtButtonPerformChange(self, event):
        if not os.path.exists(self.path_entry.GetValue()):
            dialog_ok(self, "Der angegebene Pfad existiert nicht.", "Fehler")
        else:
            # Call method to perform formatting process
            if self.change_recursive.GetValue():
                format_rec(self.path_entry.GetValue(), self.format.GetSelection(),
                    self.separation.GetSelection(), self.normal_files.GetValue(),
                    self.folders.GetValue(), self.hidden.GetValue())
            else:
                format_first(self.path_entry.GetValue(), self.format.GetSelection(),
                    self.separation.GetSelection(), self.normal_files.GetValue(),
                    self.folders.GetValue(), self.hidden.GetValue())

    def EvtPathButton(self, event):
        self.path_entry.SetValue(dialog_dir(self, "Wählen Sie den Ordner"))

class RenameFolder(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        # Create size
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        # Create path input field
        path_input(self)
        rename_mode(self)
        word_separation(self, rename_separation_text)
        ordering_mode(self)
        sectors_num(self)
        over_sectors(self)

        self.button_perform_change = button(self, "Dateien umbenennen")
        self.Bind(wx.EVT_BUTTON, self.EvtButtonPerformChange, self.button_perform_change)

        self.sizer.Add(self.path, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.rename_mode, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.separation, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.ordering_mode, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.sectors_num, 1, wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)
        self.sizer.Add(self.over_sectors, 1, wx.EXPAND)
        self.sizer.Add(self.button_perform_change, 1, wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, 20)

        # Set sizer
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.sizer.Fit(self)
        self.Show()

    def EvtButtonPerformChange(self, event):
        if not os.path.exists(self.path_entry.GetValue()):
            dialog_ok(self, "Der angegebene Pfad existiert nicht.", "Fehler")
            return
        self.items_in_dir = len([name for name in os.listdir(self.path_entry.GetValue())])
        # Collect border numbers from user
        borders = list()
        names = list()
        num_sectors = self.sectors_num_entry.GetValue()
        # maximal border value
        max = self.items_in_dir - 1
        # minimal border value
        min = 1
        # start and end of renaming
        start = dialog_first_elem(self, min, max+1)
        end = dialog_last_elem(self, min, max+1)
        for i in range(num_sectors - 1):
            border = dialog_border(self, i+1, min, max)
            # if user cancels, exit from function
            if border == -1:
                return
            borders.append(border)
            min = border + 1
        # Collect sector numbers
        if num_sectors == 1:
            value = dialog_sector_names(self, "Bitte geben Sie den gemeinsamen Namensteil der Dateien ein")
            if value == "Eingabe":
                return
            names.append(value)
        else:
            for i in range(num_sectors):
                value = dialog_sector_names(self, "Bitte geben Sie den Namen des Sektors {0} ein".format(i+1))
                if value == "Eingabe":
                    return
                names.append(value)
        try:
            rename_files(self.path_entry.GetValue(), self.rename_mode.GetSelection(),
                self.separation.GetSelection(), num_sectors, borders, names,
                self.over_sectors.GetValue(), self.ordering_mode.GetSelection(), start, end)
        except PIL.UnidentifiedImageError:
            dialog_ok(self, "Nicht alle Dateien besitzen ein Aufnahmedatum", "Nicht geeignete Dateien gefunden")
        except NameError:
            dialog_ok(self, "Es konnte keine verwertbare Sortierung der Dateien gefunden werden", "Keine verwertbare Sortierung")


    def EvtPathButton(self, event):
        self.path_entry.SetValue(dialog_dir(self, "Wählen Sie den Ordner"))


app = wx.App(False)
frame = wx.Frame(None, title="Folder Structure", size=(1050,500))

# Create notebook
nb = wx.Notebook(frame)
nb.AddPage(OutputFiles(nb), "Dateien auflisten")
nb.AddPage(Inspect(nb), "Inspektion des Ordnerinhaltes")
nb.AddPage(FormatFolder(nb), "Ordnerinhalt formatieren")
nb.AddPage(RenameFolder(nb), "Ordnerinhalt umbenennen")

frame.Show()
app.MainLoop()
