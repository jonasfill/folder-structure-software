from enum import Enum


class SeparationMode(Enum):
    SPACE_SEPARATED = 0
    UNDERSCORE_SEPARATED = 1
    DASH_SEPARATED = 2
    NOT_SEPARATED = 3
